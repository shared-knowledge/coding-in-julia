module Kmer

export kmers, shared_kmers, kmer_overlap

const DEFAULT_KMER_LENGTH = 5

"""
Given a string, convert it to an array of kmers
"""
function kmers(str::String, k=DEFAULT_KMER_LENGTH)
  kmer_array = Array{String, 1}()
  start_i = 1
  while start_i + k - 1 <= length(str)
    push!(kmer_array, str[start_i:start_i+k-1])
    start_i += k
  end
  return kmer_array
end

"""
Given two strings, find all the common kmers
"""
function shared_kmers(str1::String, str2::String, k=DEFAULT_KMER_LENGTH)
  kmers1 = kmers(str1, k)
  kmers2 = kmers(str2, k)
  shared = Array{String, 1}()
  for kmer in kmers1
    if kmer in kmers2
      push!(shared, kmer)
    end
  end
  return shared
end

"""
Given n strings, return a "distance"/"overlap" matrix
"""
function kmer_overlap(str_array::Array{String}, k=DEFAULT_KMER_LENGTH)
  return [ length(shared_kmers(i, j, k)) for i in str_array, j in str_array]
end

end

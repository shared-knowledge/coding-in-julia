# coding-test
## Coding Interview Challenges

### Get started
First things first, `git clone` this repository onto your machine (the repository address can be found in the green "Clone or download" button on github).

If you want to start with the challenges, open Challenges.ipynb in an IJulia Notebook. [Get IJulia here if you haven't already](https://github.com/JuliaLang/IJulia.jl)

### Save your progress
As you make progress on the challenges, commit all your changes 
(including any additional files and code and comments) to the repo, and 
don't forget to push! We'll be able to monitor your progress through 
your commits. (Also, use the commit messages to tell us what you have 
and haven't done yet).

### What was I supposed to do again
Read the instructions again at https://gist.github.com/murrellb/820644db11539939e1555f7182ede1d1
